# Создаём виртуальные машины "web##" и определяем конфигурацию
resource "yandex_compute_instance" "web" {
  count = "${var.web_count}"
  name = "${var.web_hostname}${format("%02d",count.index + 1)}"
  labels = {
    ansible_group = "web"
  }
  hostname = "${var.web_hostname}${format("%02d",count.index + 1)}"
  resources {
    cores  = "${var.web_cores_count}"
    memory = "${var.web_memory_amount}"
    core_fraction = "${var.web_core_fraction}"
  }
  
  boot_disk {
    initialize_params {
      image_id = "${var.web_image_id}"
	  size = "${var.web_disk_size}"
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    nat       = "${var.web_nat}"
  }

  scheduling_policy {
    preemptible   = true
  }

  metadata = {
    ssh-keys = "${var.ssh_username}:${file(var.public_key_path)}"
  }
}

# Создаём виртуальные машины "files##" и определяем конфигурацию
resource "yandex_compute_instance" "files" {
  count = "${var.files_count}"
  name = "${var.files_hostname}${format("%02d",count.index + 1)}"
  labels = {
    ansible_group = "files"
  }
  hostname = "${var.files_hostname}${format("%02d",count.index + 1)}"
  resources {
    cores  = "${var.files_cores_count}"
    memory = "${var.files_memory_amount}"
    core_fraction = "${var.files_core_fraction}"
  }
  
  boot_disk {
    initialize_params {
      image_id = "${var.files_image_id}"
	  size = "${var.files_disk_size}"
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    nat       = "${var.files_nat}"
  }

  scheduling_policy {
    preemptible   = true
  }

  metadata = {
    ssh-keys = "${var.ssh_username}:${file(var.public_key_path)}"
  }
}


# Создаём публичную зону DNS
resource "yandex_dns_zone" "zone1" {
  name    = "${var.domain_name}"
  zone    = "${var.domain_zone}"
  public  = true
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.${var.domain_zone}"
  type    = "A"
  ttl     = 200
  data    = ["${yandex_compute_instance.web[0].network_interface.0.nat_ip_address}"]
}

resource "yandex_dns_recordset" "rs2" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "${var.domain_zone}"
  type    = "A"
  ttl     = 200
  data    = ["${yandex_compute_instance.web[0].network_interface.0.nat_ip_address}"]
}

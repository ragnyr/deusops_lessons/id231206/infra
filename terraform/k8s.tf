# # Network
# resource "yandex_vpc_network" "k8s-network" {
#   name = "k8s-network"
# }

# resource "yandex_vpc_subnet" "k8s-subnet-a" {
#   name           = "k8s-subnet-a"
#   zone           = "${var.zone}"
#   network_id     = "${yandex_vpc_network.network-1.id}"
#   v4_cidr_blocks = ["192.168.10.0/24"]
#   depends_on = [
#     yandex_vpc_network.k8s-network,
#   ]
# }

# KMS
resource "yandex_kms_symmetric_key" "k8s-key" {
  name              = "k8s-key"
  default_algorithm = "AES_256"
  rotation_period   = "8760h"
}

resource "yandex_kubernetes_cluster" "k8s-id231206" {
  name        = "k8s-id231206"
  description = "Cluster for lesson id231206"

  network_id = "${yandex_vpc_network.network-1.id}"

  master {
    version = "1.28"
    zonal {
        zone      = "${yandex_vpc_subnet.subnet-1.zone}"
        subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    }

    security_group_ids = [
      yandex_vpc_security_group.k8s-main-sg.id,
      yandex_vpc_security_group.k8s-master-whitelist.id
    ]

    public_ip = true

    maintenance_policy {
      auto_upgrade = false
    }
  }

  service_account_id      = "${yandex_iam_service_account.folderEditor.id}"
  node_service_account_id = "${yandex_iam_service_account.k8s-node-sa.id}"

  release_channel = "STABLE"
  network_policy_provider = "CALICO"

  kms_provider {
    key_id = yandex_kms_symmetric_key.k8s-key.id
  }
}

# Workers node
resource "yandex_kubernetes_node_group" "k8s_workers" {
  cluster_id  = yandex_kubernetes_cluster.k8s-id231206.id
  name        = "workers"
  description = "description"
  version     = "1.28"

  instance_template {
    name = "worker-{instance.index}"
    platform_id = "standard-v2"
    # network_interface {
    #   nat                = true
    #   subnet_ids         = [yandex_vpc_subnet.k8s-subnet-1.id]
    #   security_group_ids = [
    #     yandex_vpc_security_group.k8s-main-sg.id,
    #     yandex_vpc_security_group.k8s-nodes-ssh-access.id,
    #     yandex_vpc_security_group.k8s-public-services.id
    #   ]
    # }

    network_interface {
      subnet_ids = [
        yandex_vpc_subnet.subnet-1.id
      ]
      nat = true
      security_group_ids = [
        yandex_vpc_security_group.k8s-main-sg.id,
        yandex_vpc_security_group.k8s-nodes-ssh-access.id,
        yandex_vpc_security_group.k8s-public-services.id
      ]
    }

    metadata = {
      ssh-keys = "kube:${file("~/.ssh/id_rsa.pub")}"
    }

    resources {
      memory = 4
      cores  = 4
      core_fraction = 5
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 1
  }

  allocation_policy {
    location {
      zone = "${var.zone}"
    }
  }

  maintenance_policy {
    auto_upgrade = false
    auto_repair  = false
  }
}

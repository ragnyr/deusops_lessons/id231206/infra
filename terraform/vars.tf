
## ssh variables ##

variable "ssh_key_path" {
  description = "path to private ssh key"
  type        = string
  default     = "~/.ssh/id_rsa"
}

variable "public_key_path" {
  description = "path to public ssh key"
  type        = string
  default     = "~/.ssh/id_rsa.pub"
}


## cloud variables ##

variable "cloud_name" {
  description = "Cloud name"
  type = string
  default = "id231012"
}
 
variable "organization_id" {
  description = "Organization id"
  type = string
  default = ""
}

variable "billing_account_id" {
  description = "Billing account id"
  type = string
  default = ""
}


## global variables ##
variable "token" {
  description = "Cloud token"
  type = string
  default = ""
  sensitive = true
}

variable "zone" {
  description = "value"
  type = string
  default = "ru-central1-a"
}

variable "cloud_id" {
  description = "value"
  type = string
  default = ""
}

variable "folder_id" {
  description = "value"
  type = string
  default = ""
}

variable "ssh_username" {
  description = "username for VMs"
  type = string
  default = "ubuntu"
}


## web variables
variable "web_hostname" {
  description = "hostname for VMs"
  type = string
  default = "web"
}

variable "web_count" {
  description = "Count web VMs"
  type = number
  default = "1"
}

variable "web_cores_count" {
  description = "Number of CPU cores for web VMs"
  type = number
  default = "2"
}

variable "web_memory_amount" {
  description = "amount of memory, GB"
  type = number
  default = "2"
}

variable "web_core_fraction" {
  description = "value"
  type = number
  default = "20"
}

variable "web_image_id" {
  description = "value"
  type = string
  default = "fd84nt41ssoaapgql97p"
}

variable "web_disk_size" {
  description = "value"
  type = number
  default = "8"
}

variable "web_nat" {
  description = "NAT is on?"
  type    = bool
  default = true
}


## files variables
variable "files_hostname" {
  description = "hostname for VMs"
  type = string
  default = "files"
}

variable "files_count" {
  description = "Count files VMs"
  type = number
  default = "1"
}

variable "files_cores_count" {
  description = "Number of CPU cores for files VMs"
  type = number
  default = "2"
}

variable "files_memory_amount" {
  description = "amount of memory, GB"
  type = number
  default = "2"
}

variable "files_core_fraction" {
  description = "value"
  type = number
  default = "20"
}

# fd84nt41ssoaapgql97p -- ubuntu-22-04-lts-v20231211
# fd83mnmdqlojapdpoup3 -- ubuntu-20-04-lts-v20211020
# fd86dpmsasssdpesgn3j -- debian-11-v20231009
# fd87bs5724r0ngu3jlb6 -- centos-7-v20230626
variable "files_image_id" {
  description = "Boot image ID"
  type = string
  default = "fd84nt41ssoaapgql97p"
}

variable "files_disk_size" {
  description = "value"
  type = number
  default = "8"
}

variable "files_nat" {
  description = "NAT is on?"
  type    = bool
  default = true
}


## network ##
variable "network_name" {
  description = "network name"
  type = string
  default = "vms_network"
}

variable "subnet_name" {
  description = "subnet name"
  type = string
  default = "subnet1"
}

variable "v4_cidr_blocks" {
    description = "value"
    type = list(string)
    default = [ "10.129.1.0/24" ]
}


## dns variables

variable "domain_name" {
  description = "domain name"
  type = string
  default = "devtestopsru"
}

variable "domain_zone" {
  description = "domain zone"
  type = string
  default = "devtestops.ru."
}
# Создаём сеть
resource "yandex_vpc_network" "network-1" {
  name = "${var.network_name}"
}

# Создаём подсеть
resource "yandex_vpc_subnet" "subnet-1" {
  name           = "${var.subnet_name}"
  zone           = "${var.zone}"
  network_id     = "${yandex_vpc_network.network-1.id}"
  v4_cidr_blocks = "${var.v4_cidr_blocks}"
}
# Указываем, что мы хотим разворачивать окружение в Yandex.Cloud
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }

  backend "s3" {
    endpoint                    = "https://storage.yandexcloud.net/"
    region                      = "ru-central1"
    bucket                      = "deusops"
    key                         = "id231206/prod/terraform.tfstate"
    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token     = "${var.token}"
  cloud_id  = "${var.cloud_id}"
  folder_id = "${var.folder_id}"
  zone      = "${var.zone}"
}



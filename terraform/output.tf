# Вывод внешних адресов серверов по группам
output "external_ip_web" {
  value = {
    for instance in yandex_compute_instance.web :
    instance.name => instance.network_interface.0.nat_ip_address
  }
}

output "external_ip_files" {
  value = {
    for instance in yandex_compute_instance.files :
    instance.name => instance.network_interface.0.nat_ip_address
  }
}


# Создание инвентари файла
resource "local_file" "host" {
  filename = "../ansible/hosts"
  content = <<-EOT
[web]
%{ for instance in yandex_compute_instance.web ~}
${ instance.name } ansible_host=${ instance.network_interface.0.nat_ip_address } ansible_user=${ var.ssh_username } ansible_ssh_private_key_file=${ var.ssh_key_path } nfs_role=client
%{ endfor ~}

[files]
%{ for instance in yandex_compute_instance.files ~}
${ instance.name } ansible_host=${ instance.network_interface.0.nat_ip_address } ansible_user=${ var.ssh_username } ansible_ssh_private_key_file=${ var.ssh_key_path } nfs_role=server
%{ endfor ~}

  EOT
}
